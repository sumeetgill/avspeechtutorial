//
//  ViewController.swift
//  AVSpeechSynthesizerTutorial
//
//  Created by Sumeet Gill on 2017-06-28.
//  Copyright © 2017 Sumeet Gill. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet var txtSpeechText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textToSpeech(text:String) {
        
        let utterance = AVSpeechUtterance(string: text)
        utterance.voice = AVSpeechSynthesisVoice(language: Locale.preferredLanguages[0])
        
        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterance)
    }
    
    func displayErrorAlert(text:String) {
        
        let alertController = UIAlertController(title: "Error", message: text, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
     
        alertController.addAction(okButton)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnSpeakPressed(_ sender: Any) {
        
        guard let text = txtSpeechText.text, txtSpeechText.text?.isEmpty == false else {
            displayErrorAlert(text: "Please enter text to speak")
            return
        }
        
        textToSpeech(text: text)
    }
    


}



